export const BENEFIT = [
  {
    src: "https://res.cloudinary.com/dicmrrmdr/image/upload/f_auto,q_auto/v1/questify/fgdt8bbziw7cm8mhqgwn",
    title: "Credible Only Access",
    description:
      "Ensure only credible participants in the Creator's Questionnaire for reliable insights.",
  },
  {
    src: "https://res.cloudinary.com/dicmrrmdr/image/upload/f_auto,q_auto/v1/questify/qlfv2tztovjo0zcv8bb9",
    title: "Prize Certainty",
    description:
      "The prize is sealed and secured for Responders by the time Creator generate the Questionnaire.",
  },
  {
    src: "https://res.cloudinary.com/dicmrrmdr/image/upload/f_auto,q_auto/v1/questify/hxu6oedkpnjpwxdvi3et",
    title: "The Control is All Yours",
    description:
      "Discover the thrill of prize distribution: Random or Fair? The choice is in the hands of the Creator.",
  },
  {
    src: "https://res.cloudinary.com/dicmrrmdr/image/upload/f_auto,q_auto/v1/questify/eprno0r0hjiscekl0utn",
    title: "Feel the Flexibility",
    description:
      "Experience an exquisite and user-friendly interface for both Creators and Responders.",
  },
];
